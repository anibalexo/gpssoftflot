# APP PARA ACTUALIZACION DE ODOMETROS EN SOFTFLOT

Esta aplicacion cumple la funcion de actualizar odometros de vehiculos en el sistema softlfot, desde la plataforma Geotab

##### Ejecutar migraciones

    - docker-compose exec djangoserver python manage.py makemigrations
    - docker-compose exec djangoserver python manage.py migrate
    - docker-compose -f local.yml exec djangoserver python manage.py migrate

##### Ejecutar comandos sobre contenedor
    - docker exec -it {id contendor} sh

##### Desplegar aplicacion

* Crear directorio `.env` en el mismo nivel que `djangoserver`
* Crear archivo `.env_dev` dentro del directorio `.env` donde se almacenaran las credenciales de acceso

> DJANGO_SETTINGS_MODULE=`name database`
>
> DJANGO_SECRET_KEY=`user database`
> 
> POSTGRES_DB=`name database`
> 
> POSTGRES_USER=`user database`
> 
> POSTGRES_PASSWORD=`password database`
> 
> POSTGRES_HOST=`ip host database`
> 
> POSTGRES_PORT=`port host database`
> 
> SOFTFLOT_DB=`name database sqlsqserver softflot`
> 
> SOFTFLOT_USER=`user database sqlsqserver softflot`
> 
> SOFTFLOT_PASSWORD=`password database sqlsqserver softflot`
> 
> SOFTFLOT_HOST=`ip host database sqlsqserver softflot`
> 
> SOFTFLOT_PORT=`port host database sqlsqserver softflot`
>
> GEOTAB_USER=`user api sdk geotab`
> 
> GEOTAB_PASSWORD=`password api sdk geotab`
> 
> GEOTAB_DATABASE=`database api sdk geotab`
> 
> GEOTAB_SERVER=`url api sdk geotab`
>
> EMAIL_USER=`email user`
> 
> EMAIL_PASSWORD=`email password`
from django.contrib import admin

from .models import Contacto


class ContactoAdmin(admin.ModelAdmin):
    """
    Model admin para contactos
    """
    list_display = ('nombres', 'email', 'fecha_creacion', 'fecha_actualizacion')
    readonly_fields = ('fecha_creacion', 'fecha_actualizacion')
    list_per_page = 10
    ordering = ('-fecha_creacion',)


admin.site.register(Contacto, ContactoAdmin)

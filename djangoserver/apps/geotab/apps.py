from django.apps import AppConfig


class GeotabConfig(AppConfig):
    name = 'geotab'

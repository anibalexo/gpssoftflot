from django.db import models


class Contacto(models.Model):
    """
    Modelo de contactos para notificar actualizaciones
    """
    nombres = models.CharField(max_length=80)
    email = models.CharField(max_length=50, unique=True)
    fecha_creacion = models.DateTimeField(auto_now_add=True)
    fecha_actualizacion = models.DateTimeField(auto_now=True)

    class Meta:
        verbose_name = 'contacto'
        verbose_name_plural = 'contactos'

    def __str__(self):
        return "{} - {}".format(self.nombres, self.email)



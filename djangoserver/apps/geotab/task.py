from datetime import datetime

from celery.utils.log import get_task_logger
from django.contrib.auth.models import User
from django.core.mail import EmailMessage, BadHeaderError
from django.http import HttpResponse
from django.template.loader import render_to_string
from djangoserver.celery import app
from .models import Contacto

from .utils.geotab_api import obtener_lecturas
from .utils.validators import eliminar_duplicados
from ..softflot.models import Vehiculo, Medicion

_logger = get_task_logger(__name__)


@app.task
def actualizar_odometros():
    """
    Tarea asincrona para actualizar
    lectura de odometros en softflot
    """
    _logger.info("Iniciando tarea de actualizar odometros")
    date = datetime.now()
    temp_lecturas = obtener_lecturas(date.isoformat())
    if temp_lecturas is not None:
        lista_resultados = []
        lecturas = eliminar_duplicados(temp_lecturas)
        vehiculos = Vehiculo.objects.using('softflot_db').filter(activo=True, gpsactivo=True, usamedidor=True, id_medidor=True)
        for lec in lecturas:
            try:
                _vehi = vehiculos.get(gpsidentificador=lec.get('vehicleIdentificationNumber'))
                odometro_lec = lec.get('odometer', None)
                _logger.info("vehiculo: {}, lectura vehiculo: {}, lectura geotab: {}".format(_vehi.noeconomico, _vehi.lectura_medidor, odometro_lec))
                obj = {'placa': _vehi.noeconomico, 'gpsidentificador': _vehi.gpsidentificador, 'ultima_actua': _vehi.gpsfechault, 'lectura': odometro_lec,
                       'observacion': ''}
                if _vehi.lectura_medidor < odometro_lec:
                    _logger.info("Nueva lectura para {}".format(_vehi.noeconomico))
                    Medicion.objects.using('softflot_db').create(id_vehiculo=_vehi, id_vehicubicbase=_vehi.id_vehicubicbase, lectura=odometro_lec,
                                                                 lectura_medidor=odometro_lec)
                    _vehi.lectura = odometro_lec
                    _vehi.lectura_medidor = odometro_lec
                    _vehi.fechaultimalectura = date
                    _vehi.gpsfechault = date
                    _vehi.gpsmedidor = odometro_lec
                    _vehi.save(using='softflot_db')
                    obj['ultima_actua'] = date
                    obj['observacion'] = 'Lectura actualizada'
                else:
                    obj['observacion'] = 'No hay nueva lectura para este vehiculo'
                lista_resultados.append(obj)
            except Exception as e:
                _logger.error("Error, mensaje: {}".format(e))

        if len(lista_resultados) > 0:
            enviar_mail_actualizar_odometros(lista_resultados)
    else:
        _logger.error("Ocurrio un error al obtener lecturas de geotab")


def enviar_mail_actualizar_odometros(lista_registros):
    """
    Tarea asíncrona que envía un email a los administradores
    con resultados al actualizar odometros de vehiculos en softflot
    :return:
    """
    print("Enviando...")
    enviado = True
    columnas_template = ["Vehiculo", "Identificador GPS", "Lectura", "Última Actualización", "Observación"]
    fecha = datetime.now()
    context = {"lista_registros": lista_registros, "columnas_template": columnas_template, "fecha": fecha}
    contactos = Contacto.objects.all()
    emails = [contacto.email for contacto in contactos]
    subject = "Reporte Geotab-Softflot"
    html_content = render_to_string('emails/resultado_odometros.html', context)
    email = EmailMessage(subject, html_content)
    email.content_subtype = "html"
    email.bcc = emails
    if not contactos:
        _logger.error("No existen usuarios para enviar reporte")
    try:
        email.send()
    except BadHeaderError:
        _logger.info("No se ha enviado el correo")
        return HttpResponse('Invalid header found.')
    return enviado

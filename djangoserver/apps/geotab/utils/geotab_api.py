import os

import mygeotab
from celery.utils.log import get_task_logger

_logger = get_task_logger(__name__)

api = mygeotab.API(username=os.environ.get('GEOTAB_USER'), password=os.environ.get('GEOTAB_PASSWORD'), database=os.environ.get('GEOTAB_DATABASE'),
                   server=os.environ.get('GEOTAB_SERVER'))
api.authenticate()
results = []
calls = []


def generate_request(devices, date):
    """
    Genera peticiones para ejecutar una sola peticion api
    :param devices: lista de dispositivos
    :return: None
    """
    diagnostic = {
        "id": "DiagnosticOdometerAdjustmentId"
    }
    for device in devices:
        results.append(
            {
                "name": device.get('name', None),
                "device_id": device.get('id', None),
                "odometer": None,
                "vehicleIdentificationNumber": device.get('vehicleIdentificationNumber', None)
            }
        )
        calls.append(
            {
                "method": "Get",
                "params": {
                    "typeName": "StatusData",
                    "search": {
                        "fromDate": date,
                        "toDate": date,
                        "diagnosticSearch": diagnostic,
                        "deviceSearch": {"id": device.get("id")}
                    }
                }
            }
        )


def update_result_json(data, result_json):
    """
    Actualiza json resultados con lecturas de odometros
    :param data: respuesta de endpoint (ExecuteMultiCall)
    :param result_json: json con datos de vehiculos
    :return: json
    """
    _response = data
    for item in _response:
        statusdata = item[0]
        _odometer = statusdata.get('data', None)
        _id_device = statusdata.get('device').get('id', None)
        if _odometer is not None and _id_device is not None:
            for rs in result_json:
                if _id_device == rs.get('device_id'):
                    rs['odometer'] = int(_odometer / 1000)

    return result_json


def obtener_lecturas(date):
    """
    Obtiene json con lecturas de odometros
    :return: json
    """
    _result = None
    try:
        generate_request(api.call("Get", typeName="Device", search={"groups": [{"id": "GroupCompanyId"}]}), date)
        _result = update_result_json(api.call("ExecuteMultiCall", calls=calls), results)
    except Exception as e:
        _logger.error("No se pudo conectar al api: {}".format(e))
    return _result

from datetime import datetime

from celery.utils.log import get_task_logger
from django.db import connections

_logger = get_task_logger(__name__)


def dictfetchall(cursor):
    """Retorna filas de cursor como diccionario"""
    columns = [col[0] for col in cursor.description]
    return [
        dict(zip(columns, row))
        for row in cursor.fetchall()
    ]


def funcion_ejecutar(sql):
    """
    Retorna diccionario obtenido al ejecutar un sql
    :param sql: sentencia sql para ejecutar
    :return: diccionario
    """
    cursor = connections['softflot_db'].cursor()
    try:
        cursor.execute(sql)
        return dictfetchall(cursor)
    except Exception as e:
        cursor.close()
        return False, e
    finally:
        cursor.close()


def obtener_vehiculos():
    """Retorna lista de vehiculos en softflot"""
    sql = """
    SELECT V.Id_Vehiculo, V.Id_VehicUbicBase, V.NoEconomico, V.GPSIdentificador, V.Lectura_Medidor FROM Mast_Vehiculos V WHERE V.Activo = 1 
    AND V.GPSActivo = 1 AND V.UsaMedidor = 1 AND V.Id_Medidor = 1 
    """
    return funcion_ejecutar(sql)


def actualiza_lectura_vehiculo(registros):
    """Actualiza campos de tabla vehiculo en softflot"""
    cursor = connections['softflot_db'].cursor()
    query = """
            UPDATE Mast_Vehiculos 
            SET Lectura = %s, Lectura_Medidor = %s, FechaUltimaLectura = %s, GPSFechaUlt = %s, GPSMedidor = %s WHERE GPSIdentificador = %s
            """
    date = datetime.now()
    data = [(reg.get('odometro'), reg.get('odometro'), date.strftime("%Y-%m-%d %H:%M:%S"), date.strftime("%Y-%m-%d %H:%M:%S"), reg.get('odometro'),
             reg.get('gps_ident')) for reg in registros]
    try:
        cursor.executemany(query, data)
        cursor.commit()
        _logger.info("Se actualizaron {} vehiculos".format(len(registros)))
    except Exception as e:
        cursor.close()
        _logger.error("Error al actualizar lectura de vehiculo: {}".format(e))
        return False
    finally:
        cursor.close()


def actualiza_lectura(registros):
    """Crea nuevo registro lectura odometro en softflot"""
    cursor = connections['softflot_db'].cursor()
    query = """
            INSERT INTO Hist_Mediciones (Id_Medidor, Id_Vehiculo, Id_VehicUbicBase, Fecha, Tiempo, Lectura, Lectura_Medidor, Lectura_Base, Usuario, 
            id_OrigenDato, LecturaFloat, Id_Imagen) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """
    date = datetime.now()
    data = [(1, reg.get('id_vehiculo'), reg.get('id_vehi_base'), date, date, reg.get('odometro'), reg.get('odometro'), 0,
             'Usuario GPS Import', 1, 0, None) for reg in registros]
    try:
        cursor.executemany(query, data)
        cursor.commit()
        _logger.info("Se ingresaron lecturas de {} vehiculos".format(len(registros)))
        actualiza_lectura_vehiculo(registros)
    except Exception as e:
        cursor.close()
        _logger.error("Error al ingresar lectura: {}".format(e))
        return False
    finally:
        cursor.close()


def actualiza_lectura_unica(registro, cursor):
    """Crea nuevo registro lectura odometro en softflot"""
    #cursor = connections['softflot_db'].cursor()
    query = """
            INSERT INTO Hist_Mediciones (Id_Medidor, Id_Vehiculo, Id_VehicUbicBase, Fecha, Tiempo, Lectura, Lectura_Medidor, Lectura_Base, Usuario, 
            id_OrigenDato, LecturaFloat, Id_Imagen) VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s, %s)
            """
    date = datetime.now()
    data = (1, registro.get('id_vehiculo'), registro.get('id_vehi_base'), date, date, registro.get('odometro'), registro.get('odometro'), 0,
            'Usuario GPS Import', 1, 0, None)
    try:
        cursor.execute(query, data)
        cursor.commit()
        _logger.info("Se ingreso lectura de vehiculo: {}".format(registro.get('GPSIdentificador')))
    except Exception as e:
        #cursor.close()
        _logger.error("Error al ingresar lectura: {}".format(e))
        return False
    finally:
        #cursor.close()
        pass


def actualiza_lectura_vehiculo_unica(registro, cursor):
    """Actualiza campos de tabla vehiculo en softflot"""
    #cursor = connections['softflot_db'].cursor()
    query = """
            UPDATE Mast_Vehiculos 
            SET Lectura = %s, Lectura_Medidor = %s, FechaUltimaLectura = %s, GPSFechaUlt = %s, GPSMedidor = %s WHERE GPSIdentificador = %s
            """
    date = datetime.now()
    data = (registro.get('odometro'), registro.get('odometro'), date.strftime("%Y-%m-%d %H:%M:%S"), date.strftime("%Y-%m-%d %H:%M:%S"), registro.get('odometro'),
            registro.get('gps_ident'))
    try:
        cursor.execute(query, data)
        cursor.commit()
        _logger.info("Se actualizaron campos de vehiculo: {}".format(registro.get('GPSIdentificador')))
    except Exception as e:
        #cursor.close()
        _logger.error("Error al actualizar lectura de vehiculo: {}".format(e))
        return False
    finally:
        #cursor.close()
        pass


def obtener_vehiculos_softflot(cursor):
    """Retorna lista de vehiculos en softflot"""
    sql = """
    SELECT V.Id_Vehiculo, V.Id_VehicUbicBase, V.NoEconomico, V.GPSIdentificador, V.Lectura_Medidor FROM Mast_Vehiculos V WHERE V.Activo = 1 
    AND V.GPSActivo = 1 AND V.UsaMedidor = 1 AND V.Id_Medidor = 1 
    """
    try:
        cursor.execute(sql)
        return dictfetchall(cursor)
    except Exception as e:
        #cursor.close()
        return False, e
    finally:
        pass
        #cursor.close()

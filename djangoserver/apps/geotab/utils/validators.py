

def verifica_existe_vehiculo(lista, atributo):
    """Verifica si exsite vehiculo en lista"""
    _result = None
    for item in lista:
        gps_ident = item.get('GPSIdentificador', None)
        if gps_ident is not None:
            if gps_ident == atributo:
                _result = item
        else:
            print("Error: el parametro gps identificador es nulo")
    return _result


def buscar_item(lista, atributo):
    _result = False
    for item in lista:
        gps_ident = item.get('vehicleIdentificationNumber', None)
        if gps_ident is not None:
            if gps_ident == atributo:
                _result = True
        else:
            print("Error: el atributo a comparar es nulo")
    return _result


def eliminar_duplicados(lista):
    """Elimina registros duplicados en lista"""
    _result = []
    for item in lista:
        if buscar_item(_result, item.get('vehicleIdentificationNumber')) is not True:
            _result.append(item)
    return _result

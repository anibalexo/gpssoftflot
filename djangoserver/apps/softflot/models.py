# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
# * Rearrange models' order
# * Make sure each model has one field with primary_key=True
# * Make sure each ForeignKey has `on_delete` set to the desired behavior.
# * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from datetime import datetime

from django.db import models


class Vehiculo(models.Model):
    """
    Modelo de softflot para almacenar
    datos de vehiculos
    """
    id_vehiculo = models.AutoField(db_column='Id_Vehiculo', primary_key=True) # Field name made lowercase.
    id_ruta = models.IntegerField(db_column='Id_Ruta', blank=True, null=True) # Field name made lowercase.
    id_tipovehiculo = models.IntegerField(db_column='Id_TipoVehiculo', blank=True, null=True) # Field name made lowercase.
    id_marcavehiculo = models.IntegerField(db_column='Id_MarcaVehiculo', blank=True, null=True) # Field name made lowercase.
    id_combustible = models.IntegerField(db_column='Id_Combustible', blank=True, null=True) # Field name made lowercase.
    id_combustible2 = models.IntegerField(db_column='Id_Combustible2', blank=True, null=True) # Field name made lowercase.
    id_medidor = models.IntegerField(db_column='Id_Medidor', blank=True, null=True) # Field name made lowercase.
    id_clasificacion = models.IntegerField(db_column='Id_Clasificacion', blank=True, null=True) # Field name made lowercase.
    id_tipoplaca = models.IntegerField(db_column='Id_TipoPlaca', blank=True, null=True) # Field name made lowercase.
    id_origplaca = models.IntegerField(db_column='Id_OrigPlaca', blank=True, null=True) # Field name made lowercase.
    id_departamento = models.IntegerField(db_column='Id_Departamento', blank=True, null=True) # Field name made lowercase.
    id_desglosellanta = models.IntegerField(db_column='Id_DesgloseLlanta', blank=True, null=True) # Field name made lowercase.
    id_empleado = models.IntegerField(db_column='Id_Empleado', blank=True, null=True) # Field name made lowercase.
    id_equipomp = models.IntegerField(db_column='Id_EquipoMP', blank=True, null=True) # Field name made lowercase.
    id_basemp = models.CharField(db_column='Id_BaseMP', max_length=50, blank=True, null=True) # Field name made lowercase.
    id_seguro = models.IntegerField(db_column='Id_Seguro', blank=True, null=True) # Field name made lowercase.
    id_transmision = models.IntegerField(db_column='Id_Transmision', blank=True, null=True) # Field name made lowercase.
    id_submarcavehiculo = models.IntegerField(db_column='Id_SubMarcaVehiculo', blank=True, null=True) # Field name made lowercase.
    id_empresa_servext = models.IntegerField(db_column='Id_Empresa_ServExt', blank=True, null=True) # Field name made lowercase.
    id_paquete = models.IntegerField(db_column='Id_Paquete', blank=True, null=True) # Field name made lowercase.
    id_vehicubicbase = models.IntegerField(db_column='Id_VehicUbicBase', blank=True, null=True) # Field name made lowercase.
    id_tipoverificacion = models.IntegerField(db_column='Id_TipoVerificacion', blank=True, null=True) # Field name made lowercase.
    id_archivoadjunto = models.IntegerField(db_column='Id_ArchivoAdjunto', blank=True, null=True) # Field name made lowercase.
    id_seguroespecial = models.IntegerField(db_column='Id_SeguroEspecial', blank=True, null=True) # Field name made lowercase.
    id_imagen = models.IntegerField(db_column='Id_Imagen', blank=True, null=True) # Field name made lowercase.
    id_claveflotilla = models.IntegerField(db_column='Id_ClaveFlotilla', blank=True, null=True) # Field name made lowercase.
    id_clavepresupuestal = models.IntegerField(db_column='Id_ClavePresupuestal', blank=True, null=True) # Field name made lowercase.
    id_centrodecosto = models.IntegerField(db_column='Id_CentroDeCosto', blank=True, null=True) # Field name made lowercase.
    id_comentgarantia = models.IntegerField(db_column='Id_ComentGarantia', blank=True, null=True) # Field name made lowercase.
    id_vehiculopadre = models.IntegerField(db_column='Id_VehiculoPadre', blank=True, null=True) # Field name made lowercase.
    id_tipoequipoespecial = models.IntegerField(db_column='Id_TipoEquipoEspecial', blank=True, null=True) # Field name made lowercase.
    id_comenttecnicos = models.IntegerField(db_column='Id_ComentTecnicos', blank=True, null=True) # Field name made lowercase.
    id_formadepago = models.IntegerField(db_column='Id_FormaDePago', blank=True, null=True) # Field name made lowercase.
    id_cliente = models.IntegerField(db_column='Id_Cliente', blank=True, null=True) # Field name made lowercase.
    id_combustibleclase = models.IntegerField(db_column='Id_CombustibleClase', blank=True, null=True) # Field name made lowercase.
    fechaliquidacion = models.DateTimeField(db_column='FechaLiquidacion', blank=True, null=True) # Field name made lowercase.
    fecha = models.DateTimeField(db_column='Fecha', blank=True, null=True) # Field name made lowercase.
    lectura = models.IntegerField(db_column='Lectura', blank=True, null=True) # Field name made lowercase.
    lectura_base = models.IntegerField(db_column='Lectura_Base', blank=True, null=True) # Field name made lowercase.
    lectura_medidor = models.IntegerField(db_column='Lectura_Medidor', blank=True, null=True) # Field name made lowercase.
    codigodebarras = models.CharField(db_column='CodigoDeBarras', max_length=50, blank=True, null=True) # Field name made lowercase.
    concatena = models.CharField(db_column='Concatena', max_length=200, blank=True, null=True) # Field name made lowercase.
    noeconomico = models.CharField(db_column='NoEconomico', max_length=30, blank=True, null=True) # Field name made lowercase.
    noinventario = models.CharField(db_column='NoInventario', max_length=50, blank=True, null=True) # Field name made lowercase.
    vehiculoligado = models.BooleanField(db_column='VehiculoLigado', blank=True, null=True) # Field name made lowercase.
    rendimiento = models.FloatField(db_column='Rendimiento', blank=True, null=True) # Field name made lowercase.
    modelo = models.CharField(db_column='Modelo', max_length=100, blank=True, null=True) # Field name made lowercase.
    ano = models.IntegerField(db_column='Ano', blank=True, null=True) # Field name made lowercase.
    placa = models.CharField(db_column='Placa', max_length=20, blank=True, null=True) # Field name made lowercase.
    serie_chasis = models.CharField(db_column='Serie_Chasis', max_length=50, blank=True, null=True) # Field name made lowercase.
    serie_motor = models.CharField(db_column='Serie_Motor', max_length=50, blank=True, null=True) # Field name made lowercase.
    pedimento = models.CharField(db_column='Pedimento', max_length=50, blank=True, null=True) # Field name made lowercase.
    color = models.CharField(db_column='Color', max_length=50, blank=True, null=True) # Field name made lowercase.
    fechaultimalectura = models.DateTimeField(db_column='FechaUltimaLectura', blank=True, null=True) # Field name made lowercase.
    comentarios = models.TextField(db_column='Comentarios', blank=True, null=True) # Field name made lowercase.
    activo = models.BooleanField(db_column='Activo', blank=True, null=True) # Field name made lowercase.
    valor = models.DecimalField(db_column='Valor', max_digits=19, decimal_places=4, blank=True, null=True) # Field name made lowercase.
    fechacompra = models.DateTimeField(db_column='FechaCompra', blank=True, null=True) # Field name made lowercase.
    porc_depresion = models.FloatField(db_column='Porc_Depresion', blank=True, null=True) # Field name made lowercase.
    equipoadicional = models.CharField(db_column='EquipoAdicional', max_length=100, blank=True, null=True) # Field name made lowercase.
    verificable = models.BooleanField(db_column='Verificable', blank=True, null=True) # Field name made lowercase.
    seguroinciso = models.CharField(db_column='SeguroInciso', max_length=50, blank=True, null=True) # Field name made lowercase.
    promedioanual = models.IntegerField(db_column='PromedioAnual', blank=True, null=True) # Field name made lowercase.
    ultimafecharotacion = models.DateTimeField(db_column='UltimaFechaRotacion', blank=True, null=True) # Field name made lowercase.
    asociado = models.BooleanField(db_column='Asociado', blank=True, null=True) # Field name made lowercase.
    capacidadtanque = models.IntegerField(db_column='CapacidadTanque', blank=True, null=True) # Field name made lowercase.
    capacidadtanque2 = models.IntegerField(db_column='CapacidadTanque2', blank=True, null=True) # Field name made lowercase.
    nodecilindros = models.FloatField(db_column='NoDeCilindros', blank=True, null=True) # Field name made lowercase.
    compresion = models.FloatField(db_column='Compresion', blank=True, null=True) # Field name made lowercase.
    rendimientoinverso = models.BooleanField(db_column='RendimientoInverso', blank=True, null=True) # Field name made lowercase.
    unidadrendimiento = models.CharField(db_column='UnidadRendimiento', max_length=20, blank=True, null=True) # Field name made lowercase.
    usacombustible = models.BooleanField(db_column='UsaCombustible', blank=True, null=True) # Field name made lowercase.
    permiteagregareqadic = models.BooleanField(db_column='PermiteAgregarEqAdic', blank=True, null=True) # Field name made lowercase.
    esunequipoadicional = models.BooleanField(db_column='EsUnEquipoAdicional', blank=True, null=True) # Field name made lowercase.
    seguro_endoso = models.CharField(db_column='Seguro_Endoso', max_length=50, blank=True, null=True) # Field name made lowercase.
    seguro_valorconvenido = models.DecimalField(db_column='Seguro_ValorConvenido', max_digits=19, decimal_places=4, blank=True, null=True) # Field name made lowercase.
    seguro_fechainicioendoso = models.DateTimeField(db_column='Seguro_FechaInicioEndoso', blank=True, null=True) # Field name made lowercase.
    seguro_comentarioendoso = models.CharField(db_column='Seguro_ComentarioEndoso', max_length=150, blank=True, null=True) # Field name made lowercase.
    seguro_deducible_dm1 = models.FloatField(db_column='Seguro_Deducible_DM1', blank=True, null=True) # Field name made lowercase.
    seguro_deducible_dm2 = models.DecimalField(db_column='Seguro_Deducible_DM2', max_digits=19, decimal_places=4, blank=True, null=True) # Field name made lowercase.
    seguro_deducible_robo1 = models.FloatField(db_column='Seguro_Deducible_Robo1', blank=True, null=True) # Field name made lowercase.
    seguro_deducible_robo2 = models.DecimalField(db_column='Seguro_Deducible_Robo2', max_digits=19, decimal_places=4, blank=True, null=True) # Field name made lowercase.
    seguro_deducible_rc = models.DecimalField(db_column='Seguro_DEducible_RC', max_digits=19, decimal_places=4, blank=True, null=True) # Field name made lowercase.
    maximacargapeso = models.FloatField(db_column='MaximaCargaPeso', blank=True, null=True) # Field name made lowercase.
    maximacargavolumen = models.FloatField(db_column='MaximaCargaVolumen', blank=True, null=True) # Field name made lowercase.
    descripcionequipoespecial = models.CharField(db_column='DescripcionEquipoEspecial', max_length=100, blank=True, null=True) # Field name made lowercase.
    id_medidorsec = models.IntegerField(db_column='Id_MedidorSec', blank=True, null=True) # Field name made lowercase.
    lecturafechsec = models.DateTimeField(db_column='LecturaFechSec', blank=True, null=True) # Field name made lowercase.
    lecturamedsec = models.IntegerField(db_column='LecturaMedSec', blank=True, null=True) # Field name made lowercase.
    estaentaller = models.BooleanField(db_column='EstaEnTaller', blank=True, null=True) # Field name made lowercase.
    refrigerado = models.BooleanField(db_column='Refrigerado', blank=True, null=True) # Field name made lowercase.
    bajadefinitiva = models.BooleanField(db_column='BajaDefinitiva', blank=True, null=True) # Field name made lowercase.
    velocidadpromedio = models.FloatField(db_column='VelocidadPromedio', blank=True, null=True) # Field name made lowercase.
    gpslatitud = models.CharField(db_column='GPSLatitud', max_length=50, blank=True, null=True) # Field name made lowercase.
    gpslongitud = models.CharField(db_column='GPSLongitud', max_length=50, blank=True, null=True) # Field name made lowercase.
    gpsfechault = models.DateTimeField(db_column='GPSFechaUlt', blank=True, null=True) # Field name made lowercase.
    gpsmedidor = models.IntegerField(db_column='GPSMedidor', blank=True, null=True) # Field name made lowercase.
    gpsdescripcion = models.CharField(db_column='GPSDescripcion', max_length=100, blank=True, null=True) # Field name made lowercase.
    campopersonalizado1 = models.CharField(db_column='CampoPersonalizado1', max_length=50, blank=True, null=True) # Field name made lowercase.
    campopersonalizado2 = models.CharField(db_column='CampoPersonalizado2', max_length=50, blank=True, null=True) # Field name made lowercase.
    campopersonalizado3 = models.CharField(db_column='CampoPersonalizado3', max_length=50, blank=True, null=True) # Field name made lowercase.
    campopersonalizado4 = models.CharField(db_column='CampoPersonalizado4', max_length=50, blank=True, null=True) # Field name made lowercase.
    tarjetadecombustible = models.CharField(db_column='TarjetaDeCombustible', max_length=50, blank=True, null=True) # Field name made lowercase.
    usuariocombregion = models.CharField(db_column='UsuarioCombRegion', max_length=50, blank=True, null=True) # Field name made lowercase.
    grupoasignado = models.CharField(db_column='GrupoAsignado', max_length=50, blank=True, null=True) # Field name made lowercase.
    territorio = models.CharField(db_column='Territorio', max_length=50, blank=True, null=True) # Field name made lowercase.
    numerotarjetaiave = models.CharField(db_column='NumeroTarjetaIAVE', max_length=50, blank=True, null=True) # Field name made lowercase.
    imagenindex = models.IntegerField(db_column='ImagenIndex', blank=True, null=True) # Field name made lowercase.
    id_estatusvehiculo = models.IntegerField(db_column='Id_EstatusVehiculo', blank=True, null=True) # Field name made lowercase.
    referenciaestatus = models.CharField(db_column='ReferenciaEstatus', max_length=255, blank=True, null=True) # Field name made lowercase.
    id_subestatusinactivo = models.IntegerField(db_column='Id_SubEstatusInactivo', blank=True, null=True) # Field name made lowercase.
    factura = models.CharField(db_column='Factura', max_length=25, blank=True, null=True) # Field name made lowercase.
    gpsactivo = models.BooleanField(db_column='GPSActivo', blank=True, null=True) # Field name made lowercase.
    gpsidentificador = models.CharField(db_column='GPSIdentificador', max_length=50, blank=True, null=True) # Field name made lowercase.
    selecciontmpsino = models.BooleanField(db_column='SeleccionTmpSiNo', blank=True, null=True) # Field name made lowercase.
    fechaultimochklist = models.DateTimeField(db_column='FechaUltimoChkList', blank=True, null=True) # Field name made lowercase.
    lecturasec = models.FloatField(db_column='LecturaSec', blank=True, null=True) # Field name made lowercase.
    lecturabasesec = models.FloatField(db_column='LecturaBaseSec', blank=True, null=True) # Field name made lowercase.
    lecturamedidorsec = models.FloatField(db_column='LecturaMedidorSec', blank=True, null=True) # Field name made lowercase.
    gpslecturasec = models.FloatField(db_column='GPSLecturaSec', blank=True, null=True) # Field name made lowercase.
    gpslecturasecfecha = models.DateTimeField(db_column='GPSLecturaSecFecha', blank=True, null=True) # Field name made lowercase.
    id_vehiculotracto = models.IntegerField(db_column='Id_VehiculoTracto', blank=True, null=True) # Field name made lowercase.
    utilizaplaca = models.BooleanField(db_column='UtilizaPlaca', blank=True, null=True) # Field name made lowercase.
    usamedidor = models.BooleanField(db_column='UsaMedidor', blank=True, null=True) # Field name made lowercase.
    usamedidorsec = models.BooleanField(db_column='UsaMedidorSec', blank=True, null=True) # Field name made lowercase.
    numpermisosct = models.CharField(db_column='NumPermisoSCT', max_length=40, blank=True, null=True) # Field name made lowercase.
    permsct = models.CharField(db_column='PermSCT', max_length=15, blank=True, null=True) # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Mast_Vehiculos'
        verbose_name = 'vehiculo'
        verbose_name_plural = 'vehiculos'
        ordering = ['id_vehiculo']

    def __str__(self):
        return "{} - {}".format(self.id_vehiculo, self.noeconomico)


class Medicion(models.Model):
    """
    Modelo de softflot para almacenar
    mediciones de lecturas de odometros
    """
    id_medicioneshist = models.AutoField(db_column='Id_MedicionesHist', primary_key=True)  # Field name made lowercase.
    id_medidor = models.IntegerField(db_column='Id_Medidor', blank=True, null=True, default=1)  # Field name made lowercase.
    id_vehiculo = models.ForeignKey('Vehiculo', models.DO_NOTHING, db_column='Id_Vehiculo', blank=True, null=True, related_name="mediciones") # Field name made lowercase.
    id_vehicubicbase = models.IntegerField(db_column='Id_VehicUbicBase', blank=True, null=True)  # Field name made lowercase.
    fecha = models.DateTimeField(db_column='Fecha', blank=True, null=True, auto_now_add=True)  # Field name made lowercase.
    tiempo = models.DateTimeField(db_column='Tiempo', blank=True, null=True, auto_now_add=True)  # Field name made lowercase.
    lectura = models.IntegerField(db_column='Lectura', blank=True, null=True)  # Field name made lowercase.
    lectura_medidor = models.IntegerField(db_column='Lectura_Medidor', blank=True, null=True)  # Field name made lowercase.
    lectura_base = models.IntegerField(db_column='Lectura_Base', blank=True, null=True, default=0)  # Field name made lowercase.
    usuario = models.CharField(db_column='Usuario', max_length=50, blank=True, null=True, default='Usuario GPS Import')  # Field name made lowercase.
    id_origendato = models.IntegerField(db_column='id_OrigenDato', blank=True, null=True, default=1)  # Field name made lowercase.
    lecturafloat = models.FloatField(db_column='LecturaFloat', blank=True, null=True, default=0)  # Field name made lowercase.
    id_imagen = models.IntegerField(db_column='Id_Imagen', blank=True, null=True, default=None)  # Field name made lowercase.

    class Meta:
        managed = False
        db_table = 'Hist_Mediciones'
        verbose_name = 'medicion'
        verbose_name_plural = 'mediciones'
        ordering = ['id_medicioneshist']

    def __str__(self):
        return "{} - {}".format(self.id_medicioneshist, self.id_vehiculo)

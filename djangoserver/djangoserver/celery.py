from celery import Celery
from django.conf import settings

# set the default Django settings module for the 'celery' program.
#os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'djangoserver.settings.local')


app = Celery('djangoserver')
app.config_from_object('django.conf:settings', namespace='CELERY')
app.autodiscover_tasks(lambda: settings.INSTALLED_APPS)
app.conf.beat_schedule = {
    'actualizar_odometros': {
        'task': 'apps.geotab.task.actualizar_odometros',
        'schedule': 3600.0,
    },
}
